from panels.storck_single_plot_control_panel import StorckSinglePlotControlPanel
from plots.single_matrix_plot import SingleMatrixPlot
from titania.QtGUI.base_tab import SimpleTab, QtBaseLayoutTab

class SingleSensorTab(SimpleTab):
    def __init__(self, parent=None, index=0, data=None, color_map=None):
        self.index = index
        QtBaseLayoutTab.__init__(self, data=data, parent=parent)
        # @TODO check if above call is correct
        self.data_to_show_first = self.data.fetch()
        self.first_date = next(iter(self.data_to_show_first[0]))
        if color_map is None:
            self.color_map = 'viridis'
        else:
            self.color_map = color_map

    def create_control_panel(self):
        return StorckSinglePlotControlPanel(widget=self, data=self.data.fetch())

    def make_plot(self):
        return SingleMatrixPlot(view=self, data=self.data_to_show_first[0][self.first_date][0].data, color_map=self.color_map,
                                title=self.data_to_show_first[0][self.first_date][0].sensor_name)
