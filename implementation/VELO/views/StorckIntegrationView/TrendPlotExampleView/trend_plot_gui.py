from pandas import np

from data.storck import MaskData
from data.storck import NoiseData
from panels.velo_control_panel import ControlPanel
from plots.storck_trend_plot import StorckTrendPlot
from views.common import SingleSensorTab, SimpleTab
from titania.plots import HistogramPlot


class TrendPlot(SimpleTab):
    def __init__(self, parent=None):
        self.parent = parent
        super().__init__(parent=parent, data=MaskData(read_trend_cache=True), control_panel=ControlPanel)
        self.data_to_show = self.data.fetch()

    def make_plot(self):
        return StorckTrendPlot(parent=self.parent, view=self, data=self.data_to_show)

    def get_title(self):
        return "Trend Example"

    def sum_numbers(self, arr):
        return np.sum(arr.data)
