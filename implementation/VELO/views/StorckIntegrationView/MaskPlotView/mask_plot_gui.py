from matplotlib.colors import ListedColormap, BoundaryNorm
# from pyqtgraph.examples.colorMaps import cmap

from data.storck import MaskData
from views.common import SingleSensorTab


class MaskPlot(SingleSensorTab):
    def __init__(self, parent=None, index=0):
        # boundaries = [0, 1]
        # norm = BoundaryNorm(boundaries, cmap.N, clip=True)
        super().__init__(parent=parent, data=MaskData(), index=index,
                         color_map=ListedColormap(['white', 'black']))

    def get_title(self):
        return "Mask"
