from data.storck import NoiseData
from views.common import SingleSensorTab


class NoisePlot(SingleSensorTab):
    def __init__(self, parent=None, index=0):
        super().__init__(parent=parent, data=NoiseData(), index=index)

    def get_title(self):
        return "Noise"
