from matplotlib.colors import ListedColormap, BoundaryNorm
# from pyqtgraph.examples.colorMaps import cmap

from data.storck import MaskData
from data.storck import PedestalFTrimStorckData
from data.storck import TrimStorckData
from views.common import SingleSensorTab


class TrimPlot(SingleSensorTab):
    def __init__(self, parent=None, index=0):
        # boundaries = [0, 1]
        # norm = BoundaryNorm(boundaries, cmap.N, clip=True)
        super().__init__(parent=parent, data=TrimStorckData(), index=index)

    def get_title(self):
        return "TrimPlot"
