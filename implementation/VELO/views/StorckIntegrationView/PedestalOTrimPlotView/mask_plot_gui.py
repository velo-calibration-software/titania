from matplotlib.colors import ListedColormap, BoundaryNorm
# from pyqtgraph.examples.colorMaps import cmap

from data.storck import MaskData
from data.storck import PedestalFTrimStorckData, \
    PedestalOTrimStorckData
from views.common import SingleSensorTab


class PedestalOTrimPlot(SingleSensorTab):
    def __init__(self, parent=None, index=0):
        # boundaries = [0, 1]
        # norm = BoundaryNorm(boundaries, cmap.N, clip=True)
        super().__init__(parent=parent, data=PedestalOTrimStorckData(), index=index)

    def get_title(self):
        return "PedestalOTrimPlot"
