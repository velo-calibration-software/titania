from pandas import np

# arr = np.random.randint(2, size=(256, 256))
#
#

arr = []
for i in range(256):
    arr_buf = []
    for j in range(255):
#creates one number out of 0 or 1 with prob p 0.4 for 0 and 0.6 for 1
        test = np.random.choice(np.arange(0, 2), p=[0.8, 0.2])
        arr_buf.append(test)
    arr.append(arr_buf)

np.savetxt("../mask_file_ref.csv", arr, delimiter=",")