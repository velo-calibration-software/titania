from data.storck.util import StorckDataHolder
from panels.velo_control_panel import ControlPanel
import pandas as pd
from pandas import np


class StorckSinglePlotControlPanel(ControlPanel):
    def __init__(self, data=None, widget=None):
        super(StorckSinglePlotControlPanel, self).__init__(data=data, widget=widget)

        self.reference_dataframe = None
        self.widget = widget
        if type(data) is [StorckDataHolder]:
            raise ValueError("Storck list is not passed")

        self.sensor_id_combo_box.clear()

        self.data = data

        first_date = next(iter(self.data[0]))

        self.data_mod = 0
        self.data_date = first_date
        self.data_to_show_on_plot = self.data[0][first_date][0]

        self.data_names = [storck_data.sensor_name for storck_data in self.data[0][first_date]]
        for name in self.data_names:
            self.sensor_id_combo_box.addItem(name)
        self.sensor_id_combo_box.setCurrentIndex(self.widget.index)
        self.sensor_id_combo_box.currentIndexChanged.connect(self.selectionchange)

        self.previous_button.clicked.connect(self.handle_button_previous)
        self.next_button.clicked.connect(self.handle_button_next)

        self.select_reference.clicked.connect(self.get_reference_file)
        self.display_references_button.stateChanged.connect(self.display_reference)
        self.data_ref_button.toggled.connect(self.subtract_data_with_reference)

        self.run_number_combo_box.clear()
        self.run_number_combo_box.setCurrentIndex(0)
        self.data_dates = self.data[0].keys()
        for date in self.data_dates:
            self.run_number_combo_box.addItem(str(date))
        self.run_number_combo_box.currentIndexChanged.connect(self.selectionchange_run_date)

        self.sensor_group_id_combo_box.clear()
        self.sensor_group_id_combo_box.setCurrentIndex(0)
        self.data_mods = self.data.keys()
        for mod in self.data_mods:
            self.sensor_group_id_combo_box.addItem(str(mod))
        self.sensor_group_id_combo_box.currentIndexChanged.connect(self.selectionchange_mod)




    def get_control_panel(self):
        return self.control_panel

    def handle_button_next(self):
        index = self.widget.index + 1
        self.sensor_id_combo_box.setCurrentIndex(index)

    def handle_button_previous(self):
        index = self.widget.index - 1
        self.sensor_id_combo_box.setCurrentIndex(index)

    def selectionchange(self, i):
        self.widget.index = i
        plot_data = self.get_data_to_show_on_plot(i)
        self.widget.plot.data_to_show = plot_data.data
        self.widget.plot.title = plot_data.sensor_name
        self.widget.plot.draw_plot()

    def selectionchange_run_date(self, i):
        self.data_date = str(self.run_number_combo_box.currentText())
        print(self.data_date)
        self.widget.index = 0
        plot_data = self.get_data_to_show_on_plot(0)

        self.data_names = [storck_data.sensor_name for storck_data in self.data[0][self.data_date]]
        self.sensor_id_combo_box.clear()
        for name in self.data_names:
            self.sensor_id_combo_box.addItem(name)

        self.widget.plot.data_to_show = plot_data.data
        self.widget.plot.title = plot_data.sensor_name
        self.widget.plot.draw_plot()

    def selectionchange_mod(self, i):
        value = int(self.sensor_group_id_combo_box.currentText())
        self.data_mod = value
        print(self.data_mod)
        first_date = next(iter(self.data[value]))
        self.data_date = first_date

        self.run_number_combo_box.clear()
        self.data_dates = self.data[self.data_mod].keys()
        for date in self.data_dates:
            self.run_number_combo_box.addItem(str(date))

        self.widget.index = 0
        plot_data = self.get_data_to_show_on_plot(0)
        self.data_names = [storck_data.sensor_name for storck_data in self.data[self.data_mod][self.data_date]]
        self.sensor_id_combo_box.clear()
        for name in self.data_names:
            self.sensor_id_combo_box.addItem(name)

        self.widget.plot.data_to_show = plot_data.data
        self.widget.plot.title = plot_data.sensor_name
        self.widget.plot.draw_plot()

    def get_reference_file(self):
        import tkinter as tk
        from tkinter import filedialog

        root = tk.Tk()
        root.withdraw()

        file_path = filedialog.askopenfilename()
        df = pd.read_csv(file_path, sep=',', header=None)
        self.reference_dataframe = df.values
        self.widget.plot.reference_data = self.reference_dataframe

        self.notification_text_box.setText("New reference file was open: {}".format(file_path))
        print(file_path)

    def display_reference(self):
        self.widget.plot.reference_data = self.reference_dataframe
        if self.display_references_button.isChecked():
            self.widget.plot.display_ref = True
        else:
            self.widget.plot.display_ref = False
        self.widget.plot.draw_plot()

    def subtract_data_with_reference(self):

        #TODO add additional checks
        if self.data_ref_button.isChecked():
            self.widget.plot.reference_data = None
            self.widget.plot.data_to_show = np.subtract(self.widget.plot.data_to_show, self.reference_dataframe)
            self.widget.plot.draw_plot()
        else:
            self.widget.plot.reference_data = self.reference_dataframe
            plot_data = self.get_data_to_show_on_plot(self.widget.index)
            self.widget.plot.data_to_show = plot_data.data
            self.widget.plot.draw_plot()

    def get_data_to_show_on_plot(self, index):
        return self.data[self.data_mod][self.data_date][index]
