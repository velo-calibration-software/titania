from data.storck.util import StorckDataUtil
from titania.data.data_core import TitaniaDataInterface

class StorckData(TitaniaDataInterface):
    def __init__(self, ids_name):
        self._sutil = StorckDataUtil()
        self.ids_name = ids_name

    def fetch(self):
        self.storck_data_list = self._sutil.get_ids(self.ids_name)
        self.storck_data_list = self._sutil.fill_storck_data_holders_with_data(self.storck_data_list)
        return self.storck_data_list


class TrimStorckData(StorckData):
    def __init__(self):
        StorckData.__init__(self, 'trim')

#TODO COLUMNS NUMBER DIFFER
class PedestalFTrimStorckData(StorckData):
    def __init__(self ):
        StorckData.__init__(self, 'pedestalFtrim')


class PedestalOTrimStorckData(StorckData):
    def __init__(self ):
        StorckData.__init__( self, 'pedestal0trim')

#TODO COLUMNS NUMBER DIFFER
class NoiseData(StorckData):
    def __init__(self ):
        StorckData.__init__( self, 'noise')


import csv

class MaskData(TitaniaDataInterface):
    def __init__(self, read_trend_cache=False):
        self.storck_util = StorckDataUtil()

        self.storck_data_list = self.storck_util.get_ids('mask')

        csvfile = open('trend.csv', 'r', newline='')
        csv_file = csv.reader(csvfile)

        if read_trend_cache:
            new_dates = []
            csv_dates = self.get_csv_dates(csv_file)
            csvfile.close()
            for storck_data_holder in self.storck_data_list:
                if str(storck_data_holder.date) not in csv_dates and str(storck_data_holder.date) not in new_dates:
                    new_dates.append(str(storck_data_holder.date))
            self.storck_data_list = [new_data for new_data in self.storck_data_list if str(new_data.date) in new_dates]

        # #filling with data
        self.storck_data_list = self.storck_util.fill_storck_data_holders_with_data(self.storck_data_list)

    def fetch(self):
        return self.storck_data_list

    def get_csv_dates(self, csv_file):
        dates_to_return = []
        for row in csv_file:
            if str(row[0]) not in dates_to_return:
                dates_to_return.append(row[0])
        return dates_to_return
