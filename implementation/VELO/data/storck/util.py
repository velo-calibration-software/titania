import requests
import json
import os, os.path
import errno
import time
from pandas import np
from storck_client import StorckClient, requests
from datetime import date, datetime


class StorckDataHolder:
    def __init__(self, data_id, data, name, date, hash_data):
        self.data_id = data_id
        self.data = data
        self.hash = hash_data
        self.name = name
        self.array_name = self.name.split('_')
        self.sensor_name = self.split_name()
        self.date = self.get_date()
        self.module_number = self.get_module_number()

    def split_name(self):
        name = self.array_name[1].replace('VP', '')
        return "SENSOR{}_ASIC{}".format(name[0], name[1])

    def get_module_number(self):
        return int(self.array_name[0].replace('Mod', ''))

    def get_date(self):
        if len(self.array_name) is 4:
            return date.fromtimestamp(int(self.array_name[-1].replace('.csv', '')))
        else:
            date_arr = self.array_name[3].split('-')
            return date(int(date_arr[0]), int(date_arr[1]), int(date_arr[2]))
            # return datetime.strptime(self.array_name[3], '%y-%m-%d').date()


class StorckDataUtil:
    workspace_token = os.environ["STORCK_WORKSPACE_TOKEN"]
    user_token = os.environ["STORCK_USER_TOKEN"]
    client = StorckClient(
        user_token = user_token,
        workspace_token = workspace_token
    )
    def __init__(self):
        # explanation:
        # you should put those into .env file and read them before running
        # self.client.auth_verify()
        pass

    def get_ids(self, name: str = ""):
        #TODO if we want to have offline version there should be saved also file with metadata, in short cache her should be also applicated
        if name is "":
            raise ValueError("Define file name")
        files_arr = self.client.get_files()
        storck_data = []
        for file in files_arr:
            filename = file['file']
            filename_arr = filename.split('_')
            operation_name = filename_arr[2]
            if name == operation_name:
                storck_data.append(StorckDataHolder(data=None, data_id=file['id'], name=file['file'], date=file['date'],
                                            hash_data=file['hash']))
        return storck_data

    def fill_storck_data_holders_with_data(self, file_ids: [StorckDataHolder] = None):
        start = time.time()
        if file_ids is None:
            raise ValueError("Pass ids to retrieve data")
        for empty_strock_data_holder in file_ids:
            if type(empty_strock_data_holder) is not StorckDataHolder:
                raise ValueError("Array element is not StorckDataHolder")

            #TODO BARDZO WAZNE, podzielic dane najpierw ze wzgledu na mod a potem, run date, moze byc to slownik, pozniej trzeba to jakos obsluzyc w controlu i ogolnie wszedzie
            is_cache = False
            is_cache = self.cache_write_data(empty_strock_data_holder, is_cache)
            self.external_source_write_data(empty_strock_data_holder, is_cache)
        end = time.time()
        print("TIME: " + str(end - start))
        return self.data_to_dic(file_ids)

    def external_source_write_data(self, empty_strock_data_holder, is_cache):
        if not is_cache:
            # if request is bad it will throw requests.exceptions.HTTPError
            data_to_fill = self.client.download_file(empty_strock_data_holder.data_id)
            empty_strock_data_holder.data = self.parse_bytestream_into_data(str(data_to_fill))
            f = self.safe_open_w("cache/{}".format(empty_strock_data_holder.name))
            f.write(str(data_to_fill))
            f.close()

    def cache_write_data(self, empty_strock_data_holder, is_cache):
        for root, dirs, files in os.walk("cache"):
            if empty_strock_data_holder.name in files:
                # print("Getting file: {} from cache".format(file.name))
                f = open(os.path.join(root, empty_strock_data_holder.name), "r")
                empty_strock_data_holder.data = self.parse_bytestream_into_data(f.read())
                is_cache = True
        return is_cache

    def parse_bytestream_into_data(self, storck_data:str):
        string_with_byte_stream = storck_data

        stream_strim = string_with_byte_stream[string_with_byte_stream.index("\\n") + 2:]
        buff = []
        iter = 0
        buff.append([])
        buff_number = ''
        for c in stream_strim:
            if c == 'n':
                # out.append(''.join(buff))
                iter = iter + 1
                buff.append([])
                buff_number = ''
            elif c == ',':
                buff[iter].append(float(buff_number))
                buff_number = ''
            elif c == '\\':
                continue
            else:
                buff_number += c
        return np.array([np.array(xi) for xi in buff[:-1]])

    def safe_open_w(self, path):
        self.mkdir_p(os.path.dirname(path))
        return open(path, 'w')

    def mkdir_p(self, path):
        try:
            os.makedirs(path)
        except OSError as exc:  # Python >2.5
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise

    def data_to_dic(self, file_ids):
        mod_list = list(set([item.module_number for item in file_ids]))
        data_list = list(set([str(item.date) for item in file_ids]))
        dic_to_return = dict.fromkeys(mod_list)
        for key, value in dic_to_return.items():
            dic_to_return[key] = dict.fromkeys(data_list)

        for key, value in dic_to_return.items():
            temp_dic = dic_to_return[key]
            for key_child, value_child in temp_dic.items():
                temp_dic[key_child] = []

        for data in file_ids:
            dic_to_return[data.module_number][str(data.date)].append(data)
        return dic_to_return
    #TODO ADD THIS LOGIC WITH DIC TO OTHER PARTS: CONTROL PLOT ETC
