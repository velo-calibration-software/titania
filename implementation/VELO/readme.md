# VELO implementation of titania

This is and implementation of titania framweork created for very basic VELO monitoring, at WFIS-AGH in Kraków.

# Installation

There mutliple ways that you can install and run this implementation.

The easiest, most advisable is to first install titania either with:
(recommended)
```
pip install git+https://gitlab.cern.ch/velo-calibration-software/titania
```

or with 
```
pip install titania
```

Then do:
```
git clone https://gitlab.cern.ch/velo-calibration-software/titania
cd titania/implementation/VELO
pip install -r requirements.txt
```



# Running

Before running this code make sure that the [storck](https://gitlab.cern.ch/velo-calibration-software/storck) service is availible on adress: 0.0.0.0:8000.

Then simply do:
```
python main.py
```

