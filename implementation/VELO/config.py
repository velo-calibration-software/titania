# from views.RUNView.PedestalView.pedestals_gui import PedestalsPlot
# from views.RUNView.PedestalsSinglePlotView.pedestals_single_plot_gui import PedestalSinglePlotWidget
# from views.RUNView.RootView.root_gui import RootView
# from views.RUNView.ScanFTrim.scan_FTrim_gui import ScanFTrimPlotWidget
# from views.RUNView.ThresholdView.thresholds_gui import AnotherThresholdsPlot
from views.StorckIntegrationView.MaskPlotView.mask_plot_gui import MaskPlot
from views.StorckIntegrationView.NoisePlotView.noise_plot_gui import NoisePlot
from views.StorckIntegrationView.PedestalFTrimPlotView.mask_plot_gui import PedestalFTrimPlot
from views.StorckIntegrationView.PedestalOTrimPlotView.mask_plot_gui import PedestalOTrimPlot
from views.StorckIntegrationView.TrendPlotExampleView.trend_plot_gui import TrendPlot
from views.StorckIntegrationView.TrimPlotView.mask_plot_gui import TrimPlot
# from views.VELOView.AreaPlotTestTab.stacked_area_plot_gui import StackedAreaQtTab
# from views.VELOView.BoxPlotView.box_plot_gui import BoxPlotTestTab
# from views.VELOView.CountPlotTestTab.count_plot_gui import CountPlotTestTab
# from views.VELOView.ScatterPlotTestTab.scatter_plot_gui import ScatterPlotTestTab
# from views.VELOView.ScatterWithHistogramPlotTestTab.scatter_with_histogram_plot_gui import \
    # ScatterWithHistogramPlotTestTab
# from views.VELOView.ThresholdView.thresholds_gui import ThresholdsTab

config = {
    # "VELOView": [ThresholdsTab, BoxPlotTestTab, StackedAreaQtTab, ScatterWithHistogramPlotTestTab, ScatterPlotTestTab,
    #              CountPlotTestTab],
    # "RUNView": [RootView, PedestalsPlot, AnotherThresholdsPlot, ScanFTrimPlotWidget, PedestalSinglePlotWidget],
    "Storck Integration": [MaskPlot, NoisePlot, PedestalFTrimPlot, PedestalOTrimPlot, TrimPlot, TrendPlot]
}
