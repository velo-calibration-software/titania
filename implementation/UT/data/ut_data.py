import os
import pandas as pd
import random
import numpy as np
import xml.etree.ElementTree as XML
from random import random
from random import seed
from titania.data.data_core import TitaniaDataInterface
from titania.QtGUI import QtBaseLayoutTab

class UTPedestalsData(TitaniaDataInterface):
    def __init__(self):

        path = '/home/physics/physusers/krupaw/Vetra/TestStave/'
        stave = "UTaXHalf0Stave0_updated"
        sensor = "TestStave"
        num_of_channels = 128
        asic_num = 4

        try:
            map_tree = XML.parse(path + stave + '.xml')
            map_tree_root = map_tree.getroot()

        except Exception as inst:
            print((" --> Problem with parsing:" + path + stave + '.xml'))
            print(" --> ABORT!")
            exit(2)

        self.data1 = Read_and_plot(map_tree, map_tree_root, 'PedestalValues', 'Pedestals_Channel', sensor)
        self.data2 = Read_and_plot(map_tree, map_tree_root, 'PedestalValues', 'Pedestals_ASIC', sensor)
        self.data3 = Read_and_plot(map_tree, map_tree_root, 'PedestalValues', 'Pedestals_Sensore', sensor)

        self.title_list = ['pedestal per  channels','pedestal per  ASIC','pedestal per sensor']
        self.x_list = ['channel', 'ASIC', 'sensor']
        self.y = ['ADC']

    def fetch(self):
        data = [self.data1, self.data2, self.data3]
        return data

    def get_x_label(self):
        return self.x_list

    def get_y_label(self):
        return self.y

    def get_title_list(self):
        return self.title_list

class UTNoiseData(TitaniaDataInterface):
    def __init__(self):

        path = '/home/physics/physusers/krupaw/Vetra/TestStave/'
        stave = "UTaXHalf0Stave0_updated"
        sensor = "TestStave"
        num_of_channels = 128
        asic_num = 4

        try:
            map_tree = XML.parse(path + stave + '.xml')
            map_tree_root = map_tree.getroot()

        except Exception as inst:
            print((" --> Problem with parsing:" + path + stave + '.xml'))
            print(" --> ABORT!")
            exit(2)

        self.data1 = Read_and_plot(map_tree, map_tree_root, 'NoiseValues', 'Noise_Channel', sensor)
        self.data2 = Read_and_plot(map_tree, map_tree_root, 'NoiseValues', 'Noise_ASIC', sensor)
        self.data3 = Read_and_plot(map_tree, map_tree_root, 'NoiseValues', 'Noise_Sensore', sensor)

        self.title_list = ['Noise per channels','Noise per ASIC','Noise per sensor']
        self.x_list = ['channel', 'ASIC', 'sensor']
        self.y = ['ADC']

    def fetch(self):
        data = [self.data1, self.data2, self.data3]
        return data

    def get_x_label(self):
        return self.x_list

    def get_y_label(self):
        return self.y

    def get_title_list(self):
        return self.title_list


class UTThresholdsData(TitaniaDataInterface):
    def __init__(self):

        path = '/home/physics/physusers/krupaw/Vetra/TestStave/'
        stave = "UTaXHalf0Stave0_updated"
        sensor = "TestStave"
        num_of_channels = 128
        asic_num = 4

        try:
            map_tree = XML.parse(path + stave + '.xml')
            map_tree_root = map_tree.getroot()

        except Exception as inst:
            print((" --> Problem with parsing:" + path + stave + '.xml'))
            print(" --> ABORT!")
            exit(2)

        self.data1 = Read_and_plot(map_tree, map_tree_root, 'NoiseValues', 'Low_Threshold', sensor)
        self.data2 = Read_and_plot(map_tree, map_tree_root, 'NoiseValues', 'High_Threshold', sensor)
        self.data3 = None

        self.title_list = ['Low Thresholds','High Thresholds','None']
        self.x_list = ['channel', 'channel', 'None']
        self.y = ['ADC']

    def fetch(self):
        data = [self.data1, self.data2, self.data3]
        return data

    def get_x_label(self):
        return self.x_list

    def get_y_label(self):
        return self.y

    def get_title_list(self):
        return self.title_list

class UTMaskData(TitaniaDataInterface):
    def __init__(self):

        path = '/home/physics/physusers/krupaw/Vetra/TestStave/'
        stave = "UTaXHalf0Stave0_updated"
        sensor = "TestStave"
        num_of_channels = 128
        asic_num = 4

        try:
            map_tree = XML.parse(path + stave + '.xml')
            map_tree_root = map_tree.getroot()

        except Exception as inst:
            print((" --> Problem with parsing:" + path + stave + '.xml'))
            print(" --> ABORT!")
            exit(2)

        self.data = Read_and_plot(map_tree, map_tree_root, 'MaskValues', 'Mask', sensor)

        self.title_list = ['Mask map channels', 'Num of masked channel in each ASIC', 'Num of masked channel in sensor']
        self.x_list = ['channel', 'ASIC', 'sensor']
        self.y = ['IsMasked','Num of masked channel','Num of masked channel']

    def fetch(self):
        return self.data 

    def get_x_label(self):
        return self.x_list

    def get_y_label(self):
        return self.y

    def get_title_list(self):
        return self.title_list

def Read_and_plot(map_tree, map_tree_root, catalog_name, parametr_name, sensor):
    try:
        for catalogs in map_tree.findall('catalog'):
            catalog = catalogs.get('name')
            if catalog == catalog_name:
                for conditions in catalogs.findall('condition'):
                    condition = conditions.get('name')
                    if condition == sensor:
                        for texts in conditions:
                            text = texts.get('name')
                            if texts.get('name') == parametr_name:
                                data_map = [float(x) for x in texts.text.split()]
        
        return data_map
    except Exception as inst:
        print((" --> Problem with parameters: {}".format(parametr_name)))
        print(" --> ABORT!")
        exit(2)
