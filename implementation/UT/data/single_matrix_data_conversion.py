import os
import pandas as pd
import random
import numpy as np
import xml.etree.ElementTree as ET
from random import random
from random import seed
from core.data.data_core import TitaniaDataInterface


class SingleMatrixDataConversion(TitaniaDataInterface):
    def __init__(self):
        dirname = os.path.dirname(__file__)
        filename = os.path.join(dirname, '../../resources/scan_FTrim.csv')
        self.arr = pd.read_csv(filename, sep=',').iloc[:, 1:].values

    def fetch(self):
        return self.arr

