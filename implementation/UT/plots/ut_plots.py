from abc import ABC, abstractmethod
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from titania.plots.base_plot import NavToolbarPlot
from titania.plots.base_plot import BaseCanvasPlot

class UTPedestalsPlot(BaseCanvasPlot):
    def get_as_plot_widget(self):
        return self

    def pre_draw(self):
        self.figure.clear()

class QtUTMultiBarPlot(NavToolbarPlot):
    def __init__(self, parent=None, view=None):
        NavToolbarPlot.__init__(self, parent=parent, view=view)

    def draw_plot(self, data=None):
        data1, data2, data3 = self.view.data.fetch()
        self.figure.clear()
        self.figure.subplots_adjust(hspace=0.3, wspace=0.3)

        ax = self.figure.add_subplot(212)
        ax.bar(range(len(data1)),data1, width=1.0, color='red')
        ax.title.set_text((self.view.data.get_title_list())[0])
        ax.set_xlabel((self.view.data.get_x_label())[0])
        ax.set_ylabel(self.view.data.get_y_label())        
        ax.set_xlim([0,len(data1)])  
        if(min(data1) >= 0):
            ax.set_ylim([0,4*max(data1)]) 
        else:  
            ax.set_ylim([4*min(data1),4*max(data1)])            
        
        ax.grid()  
        self.draw()

        ax = self.figure.add_subplot(221)
        ax.plot(data2, 'o', color='red', markersize=6)
        ax.title.set_text((self.view.data.get_title_list())[1])
        ax.set_xlabel((self.view.data.get_x_label())[1])
        ax.set_ylabel(self.view.data.get_y_label())
        ax.set_xticks([0,1,2,3])
        ax.grid()  
        self.draw()
    
        ax = self.figure.add_subplot(222)
        ax.plot(data3, 'o', color='red', markersize=6)
        ax.title.set_text((self.view.data.get_title_list())[2])
        ax.set_xlabel((self.view.data.get_x_label())[2])
        ax.set_ylabel(self.view.data.get_y_label()) 
        ax.set_xticks([0])
        ax.grid()  
        self.draw()

class QtUTDoubleLinePlot(NavToolbarPlot):
    def __init__(self, parent=None, view=None):
        NavToolbarPlot.__init__(self, parent=parent, view=view)

    def draw_plot(self, data=None):
        data1, data2, data3 = self.view.data.fetch()
        self.figure.clear()
        self.figure.subplots_adjust(hspace=0.4, wspace=0.4)

        ax1, ax2 = self.figure.subplots(2)

        ax1.bar(range(len(data1)),data1, width=1.0, color='red')
        ax1.title.set_text((self.view.data.get_title_list())[0])
        ax1.set_xlabel((self.view.data.get_x_label())[0])
        ax1.set_ylabel(self.view.data.get_y_label())        
        ax1.set_xlim([0,len(data2)])   
        ax1.set_ylim([0,2*max(data2)])            
        ax1.grid()  
        self.draw()

        ax2.bar(range(len(data2)),data2, width=1.0, color='red')
        ax2.title.set_text((self.view.data.get_title_list())[1])
        ax2.set_xlabel((self.view.data.get_x_label())[1])
        ax2.set_ylabel(self.view.data.get_y_label())        
        ax2.set_xlim([0,len(data2)])  
        ax2.set_ylim([0,2*max(data2)])            
        ax2.grid()  
        self.draw()

class QtUTSingleBarPlo2(NavToolbarPlot):
    def __init__(self, parent=None, view=None):
        NavToolbarPlot.__init__(self, parent=parent, view=view)

    def draw_plot(self, data=None):
        data = self.view.data.fetch()
        self.figure.clear()
        self.figure.subplots_adjust(hspace=0.3, wspace=0.3)

        ax = self.figure.add_subplot(1)
        ax.bar(range(len(data)),data, width=1.0, color='red')
        ax.title.set_text((self.view.data.get_title_list())[0])
        ax.set_xlabel((self.view.data.get_x_label())[0])
        ax.set_ylabel(self.view.data.get_y_label())        
        ax.set_xlim([0,len(data)])    
        ax.grid()  
        self.draw()

class QtUTMultiBarPlot2(NavToolbarPlot):
    def __init__(self, parent=None, view=None):
        NavToolbarPlot.__init__(self, parent=parent, view=view)

    def draw_plot(self, data=None):
        data_channels = self.view.data.fetch()
        data_asic = []
        for i in range(4):
            data_asic.append((data_channels [i*128:(i+1)*128-1]).count(1))
        data_sensor = data_channels .count(1)
        self.figure.clear()
        self.figure.subplots_adjust(hspace=0.3, wspace=0.3)

        ax = self.figure.add_subplot(212)
        ax.bar(range(len(data_channels)),data_channels, width=1.0, color='red')
        ax.title.set_text((self.view.data.get_title_list())[0])
        ax.set_xlabel((self.view.data.get_x_label())[0])
        ax.set_ylabel((self.view.data.get_y_label())[0])       
        ax.set_xlim([0,len(data_channels)])    
        ax.grid()  
        self.draw()

        ax = self.figure.add_subplot(221)
        ax.plot(data_asic, 'o', color='red', markersize=6)
        ax.title.set_text((self.view.data.get_title_list())[1])
        ax.set_xlabel((self.view.data.get_x_label())[1])
        ax.set_ylabel((self.view.data.get_y_label())[1])
        ax.set_xticks([0,1,2,3])
        ax.grid()  
        self.draw()
    
        ax = self.figure.add_subplot(222)
        ax.plot(data_sensor, 'o', color='red', markersize=6)
        ax.title.set_text((self.view.data.get_title_list())[2])
        ax.set_xlabel((self.view.data.get_x_label())[2])
        ax.set_ylabel((self.view.data.get_y_label())[2])
        ax.set_xticks([0])
        ax.grid()  
        self.draw()


# fun or loop

    def get_name(self):
        return "asd"

        
