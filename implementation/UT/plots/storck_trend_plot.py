from pandas import np
from datetime import date, datetime
import csv
from titania.plots.base_plot import NavToolbarPlot
from numpy import genfromtxt


class StorckTrendPlot(NavToolbarPlot):
    def __init__(self, parent=None, view=None, data=None):
        NavToolbarPlot.__init__(self, parent=parent, view=view)

        self.data_dic = {}

        #TODO mayby it is good idea to read data only from file and before that count last data and save to trend.csv
        #TODO earlier query only new data
        data_to_write = self.read_data_from_cache_and_storck(data)

        self.add_missing_data_from_cache(data_to_write)

        self.write_data_to_cache(data_to_write)

        self.data_dic = sorted(self.data_dic.items(), key=lambda x: datetime.strptime(x[0], '%Y-%m-%d'), reverse=False)
        self.data = self.data_dic

    def write_data_to_cache(self, data_to_write):
        csvfile = open('trend.csv', 'w', newline='')
        csv_file = csv.writer(csvfile)
        csv_file.writerows(data_to_write)
        csvfile.close()

    def add_missing_data_from_cache(self, data_to_write):
        csvfile = open('trend.csv', 'r', newline='')
        csv_file = csv.reader(csvfile)
        for row in csv_file:
            if row[0] not in self.data_dic:
                self.data_dic[row[0]] = float(row[1])
        csvfile.close()

    def read_data_from_cache_and_storck(self, data):
        csvfile = open('trend.csv', 'r', newline='')
        csv_file = csv.reader(csvfile)
        data_to_write = []
        dates_used = []
        for row in csv_file:
            data_to_write.append((row[0], row[1]))
            dates_used.append(str(row[0]))
        if data:
            for key, value in data[0].items():
                self.data_dic[key] = sum(storck_data.data.sum() for storck_data in value)
                if str(key) not in dates_used:
                    data_to_write.append((key, self.data_dic[key]))
        csvfile.close()
        return data_to_write

    def draw_plot(self):
        self.figure.clear()
        ax = self.figure.add_subplot(self.plot_number)
        names = []
        sum_data = []
        for single_touple in self.data:
            names.append(single_touple[0])
            sum_data.append(single_touple[1])


        x_pos = np.arange(len(names))

        ax.plot(x_pos, sum_data)
        # ax.bar(x_pos, sum_data, align='center')
        ax.set_xticks(x_pos)
        ax.set_xticklabels(names)

        self.draw()

    def key_exist_in_csv(self, key, csv_file):
        for row in csv_file:
            if key == row[0]:
                return row[1]
        return None
