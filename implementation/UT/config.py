from views.Parameters.pedestals_gui import PedestalsView
from views.Parameters.noise_gui import NoiseView
from views.Parameters.mask_gui import MaskView
from views.Parameters.thresholds_gui import ThresholdsView

config = {
    "UTView": [PedestalsView, NoiseView, ThresholdsView, MaskView]
}
