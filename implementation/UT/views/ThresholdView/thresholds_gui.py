from panels.ut_control_panel import ControlPanel
from titania.QtGUI import QtBaseLayoutTab
from titania.data.examplary_generated_data import RandomNList
from plots.ut_plot import ToolbarLinePlot
from .thresholds import Thresholds

class ThresholdsTab(QtBaseLayoutTab, Thresholds):
    def __init__(self, parent=None):
        self.parent = parent
        QtBaseLayoutTab.__init__(self, data=RandomNList(), parent=parent)
        Thresholds.__init__(self, ToolbarLinePlot)


    def add_plots_to_layout(self):
        Thresholds.add_plots_to_layout(self)

    def make_plot(self):
        return self.PlotClass(parent=self.parent, view=self)

    def create_control_panel(self):
        return ControlPanel(widget=self)
