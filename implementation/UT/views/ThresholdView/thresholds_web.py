from titania.plots import LinePlot
from .thresholds import Thresholds

class ThresholdsWeb(Thresholds):

    def make_plot(self):
        return LinePlot(parent=self)
