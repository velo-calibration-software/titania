from data.ut_data import UTPedestalsData
from panels.ut_control_panel import ControlPanel
from titania.QtGUI import QtBaseLayoutTab
from plots.ut_plots import QtUTMultiBarPlot
from .thresholds import Thresholds

class PedestalsView(QtBaseLayoutTab, Thresholds):
    def __init__(self, parent=None):
        self.parent = parent
        Thresholds.__init__(self, QtUTMultiBarPlot)
        QtBaseLayoutTab.__init__(self, data=UTPedestalsData(), parent=parent)

    def add_plots_to_layout(self):
        Thresholds.add_plots_to_layout(self,1)

    def make_plot(self):
        return self.PlotClass(parent=self.parent, view=self)

    def create_control_panel(self):
        return ControlPanel(widget=self)

    def get_title(self):
        return "Pedestals"


