from data.ut_data import UTPedestalsData
from data.ut_data import UTNoiseData
from panels.ut_control_panel import ControlPanel
from titania.QtGUI import QtBaseLayoutTab
from titania.plots import ToolbarLinePlot
from titania.plots.matrix_plot import MatrixPlot
from views.ThresholdView.thresholds import Thresholds

class PedestalsView(QtBaseLayoutTab, Thresholds):
    def __init__(self, parent=None):
        self.parent = parent
        QtBaseLayoutTab.__init__(self, data=UTNoiseData(), parent=parent)
        Thresholds.__init__(self, ToolbarLinePlot)

    def add_plots_to_layout(self):
        self.grid_layout.addLayout(self.control_panel.get_control_panel(), 0, 0)
        plot1 = self.PlotClass(parent=self.parent, view=self).get_as_plot_widget(row=1)
        plot1.draw_plot(data=(self.data)[0:127])
        self.plot_panel_grid.addWidget(plot1, 0, 0, 1, 3)
        plot2 = self.PlotClass(parent=self.parent, view=self).get_as_plot_widget(row=3)
        plot2.draw_plot(data=(self.data)[128:255])
        self.plot_panel_grid.addWidget(plot2, 2, 0, 1, 3)
        plot3 = self.PlotClass(parent=self.parent, view=self).get_as_plot_widget(row=5)
        plot3.draw_plot(data=(self.data)[256:383])
        self.plot_panel_grid.addWidget(plot3, 4, 0, 1, 3)
        plot5 = self.PlotClass(parent=self.parent, view=self).get_as_plot_widget(row=7)
        plot5.draw_plot(data=(self.data)[384:511])
        self.plot_panel_grid.addWidget(plot5, 6, 0, 1, 3)


    def make_plot(self):
        return self.PlotClass(parent=self.parent, view=self)

    def create_control_panel(self):
        return ControlPanel(widget=self)
