
import numpy as np
from titania.data.data_core import TitaniaDataInterface


class MatrixData(TitaniaDataInterface):
    def __init__(self):
        self.data = []

        self.data_map = {
            'lower_g1_s2': 0,
            'lower_g1_s5': 1,
            'upper_g0_s0': 2,
            'upper_g0_s1': 3,
            'upper_g0_s2': 4,
            'lower_g1_s1': 5,
            'lower_g1_s4': 6,
            'upper_g0_s3': 7,
            'upper_g0_s4': 8,
            'upper_g0_s5': 9,
            'lower_g1_s0': 10,
            'lower_g1_s3': 11,
            'upper_g1_s3': 12,
            'upper_g1_s0': 13,
            'lower_g0_s5': 14,
            'lower_g0_s4': 15,
            'lower_g0_s3': 16,
            'upper_g1_s4': 17,
            'upper_g1_s1': 18,
            'lower_g0_s2': 19,
            'lower_g0_s1': 20,
            'lower_g0_s0': 21,
            'upper_g1_s5': 22,
            'upper_g1_s2': 23
        }
        self.data_names = list(self.data_map.keys())
        for x in self.data_names:
            self.data.append(np.random.rand(256, 256))

    def fetch(self):
        return self
