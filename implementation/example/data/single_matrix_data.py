import os
from pathlib import Path
import pandas as pd
from titania.data.data_core import TitaniaDataInterface


class SingleMatrixData(TitaniaDataInterface):
    def __init__(self):
        curpath = Path('.').resolve().parent/'examplary_data'
        rootpath = os.environ.get('TITANIA_DATA_PATH') or curpath
        titania_data_path = Path(rootpath)
        filename = titania_data_path / 'scan_FTrim.csv'
        self.arr = pd.read_csv(filename, sep=',').iloc[:, 1:].values

    def fetch(self):
        return self.arr
