from titania.data.data_core import TitaniaDataInterface
import seaborn as sns

class IrisData(TitaniaDataInterface):
    def fetch(self):
        return sns.load_dataset('iris')
