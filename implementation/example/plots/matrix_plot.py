import config
from views.RUNView.PedestalsSinglePlotView.pedestals_single_plot_gui import PedestalSinglePlotWidget
from titania.plots import NavToolbarPlot


class MatrixPlot(NavToolbarPlot):

    def __init__(self, parent=None, view=None):
        NavToolbarPlot.__init__(self, parent=parent, view=view)
        self.plots = []
        self.parent = parent
        self.string_to_find_widget_to_replace = "PedestalSinglePlotWidget"
        self.tab_to_replace = 0
        self.is_single_plot_possible = False
        self.find_index_of_tab_to_replace()
        self.data = self.view.data.fetch()

    def find_index_of_tab_to_replace(self):
        for i in config.config["RUNView"]:
            if self.string_to_find_widget_to_replace in str(i):
                self.is_single_plot_possible = True
                break
            else:
                self.tab_to_replace += 1

    def draw_plot(self):
        self.figure.clear()
        gap = [2, 8, 14, 15, 21, 27]

        for x in range(30):
            if x not in gap:
                ax = self.figure.add_subplot(5, 6, x + 1)
                if x in [0, 1, 6, 7, 12, 13]:
                    pos1 = ax.get_position()  # get the original position
                    pos2 = [pos1.x0 + 0.03, pos1.y0, pos1.width, pos1.height]
                    ax.set_position(pos2)
                if x in [16, 17, 22, 23, 28, 29]:
                    pos1 = ax.get_position()  # get the original position
                    pos2 = [pos1.x0 - 0.03, pos1.y0, pos1.width, pos1.height]
                    ax.set_position(pos2)
                ax.set_xticks([])
                ax.set_yticks([])
                self.plots.append(ax)

        for idx, plot in enumerate(self.plots):
            plot.imshow(self.data.data[idx], interpolation='nearest', cmap='YlOrRd')
        self.mpl_connect('button_press_event', self.onclick)
        self.draw()

    def onclick(self, event):
        for i, ax in enumerate(self.plots):
            if ax == event.inaxes and self.is_single_plot_possible and self.toolbar._active is None:  # ADD LOG IF NOT POSSIBLE
                single_matrix_plot_in_new_widget = PedestalSinglePlotWidget(parent=self.parent, index=i)
                single_matrix_plot_in_new_widget.initiate()
                self.parent.child_tab.removeTab(self.tab_to_replace)
                self.parent.update()
                self.parent.child_tab.addTab(single_matrix_plot_in_new_widget, self.data.data_names[i])
                self.parent.child_tab.setCurrentIndex(self.tab_to_replace)
