from matplotlib.colors import ListedColormap
from pandas import np

from titania.plots import NavToolbarPlot


class SingleMatrixPlot(NavToolbarPlot):
    def __init__(self, parent=None, view=None, data=None, reference_data=None, display_ref=False, color_map=None,
                 title=""):
        NavToolbarPlot.__init__(self, parent=parent, view=view)
        self.index = view.index
        if data is not None and (isinstance(data, list) or data.any()):
            self.data_to_show = data
        else:
            self.data_to_show = self.view.data.data[self.index]

        self.reference_data = reference_data
        self.display_ref = display_ref

        self.color_map = color_map
        self.title = title

    def draw_plot(self):
        self.figure.clear()
        self.ax = self.figure.add_subplot(111)
        amin = np.amin(self.data_to_show)
        amax = np.amax(self.data_to_show)
        if amax == 0:
            amax = 1
        self.im = self.ax.matshow(self.data_to_show, interpolation='nearest', vmin=amin,
                                  vmax=amax, cmap=self.color_map)

        if self.reference_data is not None and self.display_ref == True:
            masked = np.ma.masked_where(self.reference_data == 0, self.reference_data)
            self.ax.matshow(masked, cmap='jet', interpolation='none', alpha=0.7)
        self.ax.set_title(self.view.get_title())
        self.figure.colorbar(self.im, ax=self.ax)

        self.draw()
