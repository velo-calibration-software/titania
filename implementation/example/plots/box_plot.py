# library & dataset
import seaborn as sns
from titania.plots.base_plot import BaseCanvasPlot, MplPlot


class BoxPlot(MplPlot):

    def __init__(self, parent=None, view=None):
        BaseCanvasPlot.__init__(self, parent=parent, view=view)
        self.data = self.view.data.fetch()

    def draw_plot(self):
        sns.boxplot(x=self.data["species"], y=self.data["sepal_length"], linewidth=5)
        self.draw()

    def get_name(self):
        return "asd"
