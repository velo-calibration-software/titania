import pandas as pd
import matplotlib.pyplot as plt
from titania.plots.base_plot import NavToolbarPlot


class ScatterPlotWithHistogram(NavToolbarPlot):
    def __init__(self, parent=None, view=None):
        NavToolbarPlot.__init__(self, parent=parent, view=view)

    def draw_plot(self, data=None):
        df = pd.read_csv("https://raw.githubusercontent.com/selva86/datasets/master/mpg_ggplot2.csv")

        grid = plt.GridSpec(4, 4, hspace=0.5, wspace=0.2)

        # Define the axes
        ax_main = self.figure.add_subplot(grid[:-1, :-1])
        ax_right = self.figure.add_subplot(grid[:-1, -1], xticklabels=[], yticklabels=[])
        ax_bottom = self.figure.add_subplot(grid[-1, 0:-1], xticklabels=[], yticklabels=[])

        ax_main.scatter('displ', 'hwy', s=df.cty * 4, c=df.manufacturer.astype('category').cat.codes, alpha=.9, data=df,
                        cmap="tab10", edgecolors='gray', linewidths=.5)

        ax_bottom.hist(df.displ, 40, histtype='stepfilled', orientation='vertical', color='deeppink')
        ax_bottom.invert_yaxis()

        ax_right.hist(df.hwy, 40, histtype='stepfilled', orientation='horizontal', color='deeppink')

        ax_main.set(title='Scatterplot with Histograms \n displ vs hwy', xlabel='displ', ylabel='hwy')
        ax_main.title.set_fontsize(20)
        for item in (
                [ax_main.xaxis.label, ax_main.yaxis.label] + ax_main.get_xticklabels() + ax_main.get_yticklabels()):
            item.set_fontsize(14)

        xlabels = ax_main.get_xticks().tolist()
        ax_main.set_xticklabels(xlabels)
