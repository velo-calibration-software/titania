import pandas as pd
import seaborn as sns
from titania.plots.base_plot import NavToolbarPlot


class CountsPlot(NavToolbarPlot):
    def __init__(self, parent=None, view=None):
        NavToolbarPlot.__init__(self, parent=parent, view=view)


    def draw_plot(self, data=None):
        df = pd.read_csv("https://raw.githubusercontent.com/selva86/datasets/master/mpg_ggplot2.csv")
        df_counts = df.groupby(['hwy', 'cty']).size().reset_index(name='counts')
        ax = self.figure.add_subplot(self.plot_number)
        sns.stripplot(df_counts.cty, df_counts.hwy,  ax=ax)
        # sns.stripplot(df_counts.cty, df_counts.hwy, size=df_counts.counts * 2, ax=ax)
        self.draw()

