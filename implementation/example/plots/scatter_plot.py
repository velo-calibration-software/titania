import seaborn as sns
from titania.plots.base_plot import NavToolbarPlot


class ScatterPlot(NavToolbarPlot):

    def __init__(self, parent=None, view=None):
        NavToolbarPlot.__init__(self, parent=parent, view=view)

    def draw_plot(self):
        data = self.view.data.fetch()
        sns.regplot(x=data["sepal_length"], y=data["sepal_width"])
        self.draw()

