from data.matrix_data import MatrixData
from panels.velo_control_panel import ControlPanel


class SinglePlotControlPanel(ControlPanel):
    def __init__(self, data=None, widget=None):
        super(SinglePlotControlPanel, self).__init__(data=data, widget=widget)

        self.widget = widget
        self.data = self.widget.data.data

        self.sensor_id_combo_box.clear()
        self.data_names = MatrixData().data_names
        for name in self.data_names:
            self.sensor_id_combo_box.addItem(name)
        self.sensor_id_combo_box.setCurrentIndex(self.widget.index)
        self.sensor_id_combo_box.currentIndexChanged.connect(self.selectionchange)

        self.previous_button.clicked.connect(self.handleButtonPrevious)
        self.next_button.clicked.connect(self.handleButtonNext)

    def get_control_panel(self):
        return self.control_panel

    def handleButtonNext(self):
        index = self.widget.index + 1
        self.sensor_id_combo_box.setCurrentIndex(index)

    def handleButtonPrevious(self):
        index = self.widget.index - 1
        self.sensor_id_combo_box.setCurrentIndex(index)

    def selectionchange(self, i):
        self.widget.index = i
        self.widget.plot.im.set_data(self.data[i])
        self.widget.plot.draw_idle()
