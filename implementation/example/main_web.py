import config_web
from titania.web.main_window import MainWindow

if __name__ == '__main__':
    main_window = MainWindow(tab_config=config_web.config)
    main_window.run()
