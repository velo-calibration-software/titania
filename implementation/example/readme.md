# Example of titania usage

This is examplary implementation made for demonstration purposes.


# Installation

There mutliple ways that you can install and run this implementation.

The easiest, most advisable is to first install titania either with:
(recommended)
```
pip install git+https://gitlab.cern.ch/velo-calibration-software/titania
```

or with 
```
pip install titania
```

Then do:
```
git clone https://gitlab.cern.ch/velo-calibration-software/titania
cd titania/implementation/example
pip install -r requirements.txt
```


# Running

Then simply do:
```
python main.py
```
