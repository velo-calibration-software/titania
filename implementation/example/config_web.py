from views.RUNView.RootView.root_web import RootWeb
from views.RUNView.ThresholdView.thresholds_web import AnotherThresholdsWeb
from views.VELOView.AreaPlotTestTab.stacked_area_plot_web import StackedAreaWeb
from views.VELOView.BoxPlotView.box_plot_web import BoxPlotWeb
from views.VELOView.CountPlotTestTab.count_plot_web import CountPlotWeb
from views.VELOView.ScatterPlotTestTab.scatter_plot_web import ScatterPlotWeb
from views.VELOView.ScatterWithHistogramPlotTestTab.scatter_with_histogram_plot_web import \
    ScatterWithHistogramPlotWeb
from views.VELOView.ThresholdView.thresholds_web import ThresholdsWeb

config = {
    "VELOView": [ThresholdsWeb, BoxPlotWeb, StackedAreaWeb, CountPlotWeb, ScatterPlotWeb, ScatterWithHistogramPlotWeb],
    "RUNView": [RootWeb, AnotherThresholdsWeb]
}
