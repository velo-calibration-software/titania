from abc import ABCMeta
import os
from pathlib import Path
from panels.velo_control_panel import ControlPanel
from titania.QtGUI import SimpleTab
from titania.common.titania_tab import TitaniaPlotTab
from titania.data.root_data import RootData
from titania.plots.root_plot import RootScatterPlot
import pathlib

class RootBase(TitaniaPlotTab):
    def __init__(self, parent=None, data_path=None):
        curpath = Path('.').resolve().parent/'examplary_data'
        rootpath = os.environ.get('TITANIA_DATA_PATH') or curpath
        titania_data_path = Path(rootpath)
        file_path =  data_path if data_path else titania_data_path/'simple.root'
        self.parent = parent
        super().__init__(data=RootData(file_path=file_path))

    def make_plot(self):
        return RootScatterPlot(parent=self, view=self)

    def get_title(self):
        return "Root histogram"
