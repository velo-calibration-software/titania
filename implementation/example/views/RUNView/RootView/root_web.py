from views.RUNView.RootView.root import RootBase
from titania.data.root_data import RootData
from titania.web.base_tab import QtTitaniaWebTab

class RootWeb(RootBase, QtTitaniaWebTab):
    def __init__(self, parent=None):
        self.parent = parent
        RootBase.__init__(self, parent=parent)
        QtTitaniaWebTab.__init__(self)

    def create_control_panel(self):
        pass
