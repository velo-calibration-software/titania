from panels.velo_control_panel import ControlPanel
from views.RUNView.RootView.root import RootBase
from titania.QtGUI import SimpleTab
from titania.data.root_data import RootData
from titania.plots.root_plot import RootScatterPlot
from titania.QtGUI.base_tab import QtBaseLayoutTab


class RootView(RootBase, QtBaseLayoutTab):
    def __init__(self, parent=None, data_path=None):
        self.parent = parent
        QtBaseLayoutTab.__init__(self, parent=self.parent)
        RootBase.__init__(self, parent=self.parent, data_path=data_path)

    def create_control_panel(self):
        return ControlPanel(widget=self)
