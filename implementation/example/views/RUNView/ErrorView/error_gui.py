from panels.velo_control_panel import ControlPanel
from views.RUNView.RootView.root_gui import RootView
from titania.QtGUI import SimpleTab

class ErrorView(RootView):
    def __init__(self, parent):
        self.parent = parent
        super().__init__(parent=self.parent, data_path='wrong_data_path_on_purpose')

    def create_control_panel(self):
        return ControlPanel(widget=self)
