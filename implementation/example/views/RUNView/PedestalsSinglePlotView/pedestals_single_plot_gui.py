from copy import deepcopy

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QSlider, QPushButton
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from data.matrix_data import MatrixData
from panels.single_plot_control_panel import SinglePlotControlPanel
from panels.velo_control_panel import ControlPanel
from plots.single_matrix_plot import SingleMatrixPlot
from views.RUNView.PedestalsSinglePlotView.pedestals_single_plot import PedestalSinglePlotBase
from titania.QtGUI import QtBaseLayoutTab


class PedestalSinglePlotWidget(PedestalSinglePlotBase, QtBaseLayoutTab):
    def __init__(self, parent=None, index=0):
        self.parent = parent
        QtBaseLayoutTab.__init__(self, parent=self.parent)
        PedestalSinglePlotBase.__init__(self, parent=self.parent, index=index)

    def create_control_panel(self):
        return ControlPanel(widget=self)

    def add_plots_to_layout(self):
        self.plot.mpl_connect('button_press_event', self.onclick)
        self.toolbar = NavigationToolbar(self.plot, self)
        self.grid_layout.addLayout(self.control_panel.get_control_panel(), 0, 0)
        self.plot_panel_grid.addWidget(self.plot, 0, 0, 1, 3)
        test_button = QPushButton("Reset")
        test_button.clicked.connect(self.handle_button)
        self.pixel_value_slider.setMinimum(0)
        self.pixel_value_slider.setMaximum(100)
        self.pixel_value_slider.setValue(40)
        self.pixel_value_slider.setTickPosition(QSlider.TicksBelow)
        self.pixel_value_slider.setTickInterval(10)
        self.plot_panel_grid.addWidget(test_button, 7, 1)
        self.plot_panel_grid.addWidget(self.pixel_value_slider, 0, 2)
        self.plot_panel_grid.addWidget(self.toolbar, 10, 1)

    def handle_button(self):
        self.plot.ax.clear()
        # self.data_to_show[0][0] = 10
        self.plot.ax.imshow(self.data_to_show, interpolation='nearest')
        self.temp_data = deepcopy(self.data_to_show)
        self.plot.draw_idle()
        # self.parent.update()

    def onclick(self, event):
        if self.toolbar._active is None:
            # self.ax.clear()
            # TODO REMEMBER ABOUT THIS LOGIC DUE TO MATHSHOW REQUIREMENTS
            # self.data_to_show[int(round(event.ydata))][int(round(event.xdata))] = 1
            self.event = event
            self.pixel_value_slider.setValue((self.temp_data[int(round(event.ydata))][int(round(event.xdata))]) * 100)
            # self.ax.imshow(self.temp_data, interpolation='nearest')

            self.pixel_value_slider.valueChanged.connect(self.valuechange)


            print(round(event.xdata), round(event.ydata))

    def valuechange(self):
        size = self.pixel_value_slider.value()
        self.temp_data[int(round(self.event.ydata))][int(round(self.event.xdata))] = (self.pixel_value_slider.value()) / 100
        self.plot.im.set_data(self.temp_data)
        self.plot.draw_idle()
