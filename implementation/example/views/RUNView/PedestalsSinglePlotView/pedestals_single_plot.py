from abc import ABCMeta
from copy import deepcopy

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QSlider, QPushButton
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from data.matrix_data import MatrixData
from panels.single_plot_control_panel import SinglePlotControlPanel
from plots.single_matrix_plot import SingleMatrixPlot
from titania.QtGUI import QtBaseLayoutTab
from titania.common.titania_tab import TitaniaPlotTab


class PedestalSinglePlotBase(TitaniaPlotTab):

    def __init__(self, parent=None, index=0):
        self.pixel_value_slider = QSlider(Qt.Vertical)
        self.index = index
        self.parent = parent
        super().__init__(data=MatrixData())
        self.data_to_show = self.data.data[self.index]
        self.temp_data = deepcopy(self.data_to_show)

    def get_title(self):
        return "PedestalSinglePlotWidget"

    def create_control_panel(self):
        return SinglePlotControlPanel(view=self)

    def make_plot(self):
        return SingleMatrixPlot(view=self)

