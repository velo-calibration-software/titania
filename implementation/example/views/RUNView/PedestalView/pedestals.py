from abc import ABCMeta

from data.matrix_data import MatrixData
from panels.velo_control_panel import ControlPanel
from plots.matrix_plot import MatrixPlot
from titania.QtGUI import SimpleTab
from titania.common.titania_tab import TitaniaPlotTab


class PedestalsPlotBase(TitaniaPlotTab):
    def __init__(self, parent=None):
        TitaniaPlotTab.__init__(self, data=MatrixData())
        self.parent = parent

    def make_plot(self):
        return MatrixPlot(parent=self.parent, view=self)

    def get_title(self):
        return "PedestalsPlot"
