from panels.velo_control_panel import ControlPanel
from views.RUNView.PedestalView.pedestals import PedestalsPlotBase
from titania.QtGUI.base_tab import QtBaseLayoutTab

class PedestalsPlot(PedestalsPlotBase, QtBaseLayoutTab):
    def __init__(self, parent=None):
        self.parent = parent
        QtBaseLayoutTab.__init__(self, parent=self.parent)
        PedestalsPlotBase.__init__(self, parent=self.parent)

    def create_control_panel(self):
        return ControlPanel(widget=self)
