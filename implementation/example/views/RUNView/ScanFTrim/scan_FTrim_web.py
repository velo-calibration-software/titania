from copy import deepcopy

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QSlider, QPushButton
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from data.single_matrix_data import SingleMatrixData
from panels.velo_control_panel import ControlPanel
from plots.single_matrix_plot import SingleMatrixPlot
from views.RUNView.ScanFTrim.scan_FTrim import ScanFTrimPlotBase
from titania.QtGUI import QtBaseLayoutTab


#TODO NOT SUITABLE FOR WEB YET
class ScanFTrimPlotWeb(ScanFTrimPlotBase):
    pass

    # def __init__(self, parent=None):
    #     self.parent = parent
    #     super().__init__()
    # def create_control_panel(self):
    #     pass
