from abc import ABCMeta
from copy import deepcopy

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QSlider, QPushButton
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from data.single_matrix_data import SingleMatrixData
from panels.velo_control_panel import ControlPanel
from plots.single_matrix_plot import SingleMatrixPlot
from views.VELOView.BoxPlotView.box_plot import BoxPlotBase
from titania.QtGUI import QtBaseLayoutTab
from titania.common.titania_tab import TitaniaPlotTab


class ScanFTrimPlotBase(TitaniaPlotTab):

    def __init__(self, parent=None, index=0):
        self.pixel_value_slider = QSlider(Qt.Vertical)
        self.index = index
        self.parent = parent
        super().__init__(data=SingleMatrixData())
        self.data_to_show = self.data.fetch()
        self.temp_data = deepcopy(self.data_to_show)

    def get_title(self):
        return "ScanFTrim"

    def create_control_panel(self):
        return ControlPanel(widget=self)

    def make_plot(self):
        return SingleMatrixPlot(view=self,data=self.data_to_show)
