from panels.velo_control_panel import ControlPanel
from views.RUNView.ThresholdView.thresholds import AnotherThresholdsBase
from titania.QtGUI import SimpleTab
from titania.data.examplary_generated_data import RandomNList
from titania.plots import HistogramPlot
from titania.QtGUI.base_tab import QtBaseLayoutTab


class AnotherThresholdsPlot(AnotherThresholdsBase, QtBaseLayoutTab):
    def __init__(self, parent=None):
        self.parent = parent
        QtBaseLayoutTab.__init__(self, parent=self.parent)
        AnotherThresholdsBase.__init__(self, parent=self.parent)

    def create_control_panel(self):
        return ControlPanel(widget=self)
