from abc import ABCMeta

from panels.velo_control_panel import ControlPanel
from titania.QtGUI import SimpleTab
from titania.common.titania_tab import TitaniaPlotTab
from titania.data.examplary_generated_data import RandomNList
from titania.plots import HistogramPlot


class AnotherThresholdsBase(TitaniaPlotTab):
    def __init__(self, parent=None):
        self.parent = parent
        super().__init__(data=RandomNList())

    def make_plot(self):
        return HistogramPlot(parent=self.parent, view=self)

    def get_title(self):
        return "AnotherThresholdsPlot"
