from panels.velo_control_panel import ControlPanel
from views.RUNView.ThresholdView.thresholds import AnotherThresholdsBase
from titania.QtGUI import SimpleTab
from titania.data.examplary_generated_data import RandomNList
from titania.plots import HistogramPlot
from titania.web.base_tab import QtTitaniaWebTab


class AnotherThresholdsWeb(AnotherThresholdsBase, QtTitaniaWebTab):
    def __init__(self, parent=None):
        self.parent = parent
        AnotherThresholdsBase.__init__(self, parent=parent)
        QtTitaniaWebTab.__init__(self)

    def create_control_panel(self):
        pass
