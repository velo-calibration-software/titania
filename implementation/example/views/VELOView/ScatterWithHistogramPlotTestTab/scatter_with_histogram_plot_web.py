from views.VELOView.ScatterWithHistogramPlotTestTab.scatter_with_histogram_plot import ScatterWithHistogramBase
from titania.web.base_tab import WebMetaclass


class ScatterWithHistogramPlotWeb(ScatterWithHistogramBase, metaclass=WebMetaclass):
    pass
