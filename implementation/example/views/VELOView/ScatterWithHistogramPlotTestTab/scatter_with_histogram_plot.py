from abc import ABCMeta

from panels.velo_control_panel import ControlPanel
from titania.QtGUI import SimpleTab
from titania.common.titania_tab import TitaniaPlotTab
from plots.scatter_with_histogram_plot import ScatterPlotWithHistogram


class ScatterWithHistogramBase(TitaniaPlotTab):
    def __init__(self, parent=None):
        self.parent = parent

    def make_plot(self):
        return ScatterPlotWithHistogram(parent=self.parent, view=self)

    def get_title(self):
        return "ScatterWithHistogramPlotExample"
