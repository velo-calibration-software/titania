from panels.velo_control_panel import ControlPanel
from views.VELOView.ScatterWithHistogramPlotTestTab.scatter_with_histogram_plot import ScatterWithHistogramBase
from titania.QtGUI import SimpleTab
from plots.scatter_with_histogram_plot import ScatterPlotWithHistogram
from titania.QtGUI.base_tab import QtBaseLayoutTab

class ScatterWithHistogramPlotTestTab(ScatterWithHistogramBase, QtBaseLayoutTab):
    def __init__(self, parent=None):
        self.parent = parent
        ScatterWithHistogramBase.__init__(self, parent=self.parent)
        QtBaseLayoutTab.__init__(self, parent=self.parent)

    def create_control_panel(self):
        return ControlPanel(widget=self)
