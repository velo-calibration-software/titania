from abc import ABCMeta

# from titania.common.titania_tab import TitaniaPlotTab
from titania.common.titania_tab import TitaniaPlotTab
from data.iris_data import IrisData
from plots.stacked_area_plot import StackedAreaPlot


class StackedAreaBase(TitaniaPlotTab):
    def __init__(self, parent=None):
        # self.data = IrisData()
        self.parent = parent
        TitaniaPlotTab.__init__(self, IrisData())

    def make_plot(self):
        return StackedAreaPlot(parent=self.parent, view=self)

    def get_title(self):
        return "StackedAreaPlotExample"
