from panels.velo_control_panel import ControlPanel
from views.VELOView.AreaPlotTestTab.stacked_area_plot import StackedAreaBase
from titania.QtGUI.base_tab import QtBaseLayoutTab
from plots.stacked_area_plot import StackedAreaPlot


class StackedAreaQtTab(StackedAreaBase,QtBaseLayoutTab):
    def __init__(self, parent=None):
        self.parent = parent
        StackedAreaBase.__init__(self, parent=self.parent)
        QtBaseLayoutTab.__init__(self, parent=self.parent)

    def create_control_panel(self):
        return ControlPanel(widget=self)
