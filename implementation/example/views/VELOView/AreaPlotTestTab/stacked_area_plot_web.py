from panels.velo_control_panel import ControlPanel
from views.VELOView.AreaPlotTestTab.stacked_area_plot import StackedAreaBase
from titania.QtGUI import SimpleTab
from plots.stacked_area_plot import StackedAreaPlot
from titania.web.base_tab import QtTitaniaWebTab
from titania.web.base_tab import WebMetaclass
from titania.common.titania_tab import TitaniaPlotTab


# class StackedAreaWeb(StackedAreaBase,  QtTitaniaWebTab):
#     def __init__(self, parent=None):
#         self.parent = parent
#         StackedAreaBase.__init__(self, parent=parent)
#         QtTitaniaWebTab.__init__(self)

#     def create_control_panel(self):
#         pass

class StackedAreaWeb(StackedAreaBase,  metaclass= WebMetaclass):
    pass
