from abc import ABCMeta

from panels.velo_control_panel import ControlPanel
from titania.QtGUI import SimpleTab
from titania.common.titania_tab import TitaniaPlotTab
from titania.common.titania_tab import TitaniaPlotTab
from titania.data.data_core import EmptyTitaniaData
from data.iris_data import IrisData
from plots.counts_plot import CountsPlot


class CountPlotBase(TitaniaPlotTab):
    def __init__(self, data=None, parent=None):
        self.parent = parent
        super().__init__(data=IrisData())

    def make_plot(self):
        return CountsPlot(parent=self.parent, view=self)

    def get_title(self):
        return "CountPlotExample"
