from panels.velo_control_panel import ControlPanel
from views.VELOView.CountPlotTestTab.count_plot import CountPlotBase
from titania.QtGUI import SimpleTab
from plots.counts_plot import CountsPlot
from titania.web.base_tab import WebMetaclass


class CountPlotWeb(CountPlotBase, metaclass=WebMetaclass):
    pass
