from panels.velo_control_panel import ControlPanel
from views.VELOView.CountPlotTestTab.count_plot import CountPlotBase
from titania.QtGUI import SimpleTab
from plots.counts_plot import CountsPlot
from titania.QtGUI.base_tab import QtBaseLayoutTab


class CountPlotTestTab(CountPlotBase, QtBaseLayoutTab):
    def __init__(self, parent=None):
        self.parent = parent
        CountPlotBase.__init__(self, parent=self.parent)
        QtBaseLayoutTab.__init__(self, parent=self.parent)

    def create_control_panel(self):
        return ControlPanel(widget=self)
