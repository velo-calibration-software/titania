from abc import ABCMeta

from titania.common.titania_tab import TitaniaPlotTab
from titania.common.titania_tab import TitaniaPlotTab
from titania.data.examplary_generated_data import RandomNList
from titania.plots import LinePlot


class ThresholdsBase(TitaniaPlotTab):
    def __init__(self, data=None, parent=None):
        self.parent = parent
        TitaniaPlotTab.__init__(self, data=RandomNList())

    def make_plot(self):
        return LinePlot(parent=self, view=self)

    def get_title(self):
        return "ThresholdsTab"

