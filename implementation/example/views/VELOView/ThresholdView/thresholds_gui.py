from panels.velo_control_panel import SharedVeloPanel
from views.VELOView.ThresholdView.thresholds import ThresholdsBase
from titania.QtGUI import QtBaseLayoutTab
from titania.data.examplary_generated_data import RandomNList
from titania.plots import ToolbarLinePlot
from titania.QtGUI.base_tab import QtBaseLayoutTab


class ThresholdsTab(ThresholdsBase, QtBaseLayoutTab):
    def __init__(self, parent=None):
        self.parent = parent
        QtBaseLayoutTab.__init__(self, parent=self.parent)
        ThresholdsBase.__init__(self, parent=self.parent)

    def create_control_panel(self):
        return SharedVeloPanel(widget=self)
