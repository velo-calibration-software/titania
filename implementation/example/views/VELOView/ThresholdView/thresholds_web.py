from views.VELOView.ThresholdView.thresholds import ThresholdsBase
from titania.web.base_tab import WebMetaclass

class ThresholdsWeb(ThresholdsBase, metaclass=WebMetaclass):
    pass
