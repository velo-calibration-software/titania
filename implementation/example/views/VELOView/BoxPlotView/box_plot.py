from abc import abstractmethod, ABCMeta

from panels.velo_control_panel import ControlPanel
from titania.QtGUI import QtBaseLayoutTab
from titania.common.titania_tab import TitaniaPlotTab
from titania.common.titania_tab import TitaniaPlotTab
from data.iris_data import IrisData
from titania.panels import EmptyControlPanel
from plots.box_plot import BoxPlot


class BoxPlotBase(TitaniaPlotTab):
    def __init__(self, data=None, parent=None):
        TitaniaPlotTab.__init__(self, data=IrisData())
        self.parent = parent

    def make_plot(self):
        return BoxPlot(parent=self, view=self)

    def get_title(self):
        return "BoxPlotExample"

