from panels.velo_control_panel import SharedVeloPanel
from views.VELOView.BoxPlotView.box_plot import BoxPlotBase
from titania.QtGUI import SimpleTab
from data.iris_data import IrisData
from plots.box_plot import BoxPlot
from titania.QtGUI.base_tab import QtBaseLayoutTab


class BoxPlotTestTab(BoxPlotBase, QtBaseLayoutTab):
    def __init__(self, parent=None):
        self.parent = parent
        QtBaseLayoutTab.__init__(self, parent=self.parent)
        BoxPlotBase.__init__(self, parent=self.parent)

    def create_control_panel(self):
        return SharedVeloPanel(widget=self)
