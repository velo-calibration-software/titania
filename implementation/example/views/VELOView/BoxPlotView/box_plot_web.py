from views.VELOView.BoxPlotView.box_plot import BoxPlotBase
from titania.web.base_tab import WebMetaclass


class BoxPlotWeb(BoxPlotBase, metaclass=WebMetaclass):
    pass
