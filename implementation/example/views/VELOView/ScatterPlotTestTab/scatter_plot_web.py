from views.VELOView.ScatterPlotTestTab.scatter_plot import ScatterPlotBase
from titania.web.base_tab import WebMetaclass


class ScatterPlotWeb(ScatterPlotBase, metaclass=WebMetaclass):
    pass
