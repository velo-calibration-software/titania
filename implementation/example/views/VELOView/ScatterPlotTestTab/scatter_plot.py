from abc import ABCMeta

from panels.velo_control_panel import ControlPanel
from titania.QtGUI import SimpleTab
from titania.common.titania_tab import TitaniaPlotTab
from data.iris_data import IrisData
from plots.scatter_plot import ScatterPlot


class ScatterPlotBase(TitaniaPlotTab):
    def __init__(self, parent=None):
        super().__init__(data=IrisData())
        self.parent = parent

    def make_plot(self):
        return ScatterPlot(parent=self.parent, view=self)

    def get_title(self):
        return "ScatterPlotExample"
