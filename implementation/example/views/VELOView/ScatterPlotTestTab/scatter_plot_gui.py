from panels.velo_control_panel import ControlPanel
from views.VELOView.ScatterPlotTestTab.scatter_plot import ScatterPlotBase
from titania.QtGUI import SimpleTab
from data.iris_data import IrisData
from plots.scatter_plot import ScatterPlot
from titania.QtGUI.base_tab import QtBaseLayoutTab


class ScatterPlotTestTab(ScatterPlotBase, QtBaseLayoutTab):
    def __init__(self, parent=None):
        self.parent = parent
        QtBaseLayoutTab.__init__(self, parent=self.parent)
        ScatterPlotBase.__init__(self, parent=self.parent)

    def create_control_panel(self):
        return ControlPanel(widget=self)
