from PyQt5.QtCore import QObject

class TitaniaSingleton(type(QObject), type):
    """
    Because the objects in Qt cannod be shared, an easy solution to the problem of sharing
    the control panel with multiple tabs is making a singleton object with signals that will
    be propagated for all subsequent objects.

    For usage see implementation/examples/panels .

    Solution from https://stackoverflow.com/a/59459994
    """

    def __init__(cls, name, bases, dict):
        super().__init__(name, bases, dict)
        cls._instance = None

    def __call__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super().__call__(*args, **kwargs)
        return cls._instance
