import inspect

import mpld3
from flask import render_template
from markupsafe import Markup
from abc import ABC, abstractmethod
from titania.common.titania_tab import TitaniaPlotTab
from titania.common.interface_tab import WebTab


class QtTitaniaWebTab(WebTab):
    """
    Subclass this along with :py:class:`titania.common.titania_tab.TitaniaPlotTab` to implement web view.
    """

    def create_control_panel(self):
        pass

class WebMetaclass(type(TitaniaPlotTab)):
    """
    Use this metaclass to create web view.
    """
    def __new__(cls, clsname, base, attrs):
        bases = (QtTitaniaWebTab,) + base
        def new_init(self, *args, **kwargs):
            QtTitaniaWebTab.__init__(self)
            base[0].__init__(self, *args, **kwargs)
        attrs['__init__'] = new_init
        cls = type.__new__(cls, clsname, bases, attrs)
        return cls

class QtWebTabFactory:
    """
    A factory class used to convert the Qt Widget plot to actual html
    """
    def build(self, cls,widget_object):
        def innerdynamo():
            widget_object.initiate()
            if widget_object.plot is not None:
                html = mpld3.fig_to_html(widget_object.plot.figure)
            else:
                html = ""
            return render_template('index.html', plot_dict=Markup(html))
        innerdynamo.__name__ = widget_object.get_title()
        setattr(cls, innerdynamo.__name__, innerdynamo)
