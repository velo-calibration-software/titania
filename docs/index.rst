.. Titania documentation master file, created by
   sphinx-quickstart on Mon Nov 22 09:59:02 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Titania's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :hidden:

   install
   start
   tutorial
   web
   API <titania.rst>
   control panel (advanced) <control_panel.rst>
   GitLab Repository <https://gitlab.cern.ch/velo-calibration-software/titania>

Although writing scripts in python that will show the data on the plot is quite easy, coordinating larger efforts that produce larger ammounts of plots is quite tricky.
But why is that?

We believe that it is because there is a lack of formal structure for such developement. This is why we created titania as a monitoring framework.


Here are some minimal and fast demo of an app created with Titania:

.. image:: https://s10.gifyu.com/images/ezgif.com-gif-maker29235c9d3921d051.gif

Indices and tables
==================

* :ref:`genindex`
* :ref:`API <modindex>`
* :ref:`search`


