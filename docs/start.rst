
Quick start
==================

This is a very quick guide for using titania framework.
(This will not work with the developer installation)

Starting the project
--------------------------------

Ok, there is no time to waste here.

Quick, make a new titania project:

.. code-block:: bash

    $ python -m titania start --name ProjectName


Run the template
--------------------------------

You just did two things;
 - started a Titania project
 - populated empty project with template implementation

Congratulations! You are now ready to run the project;
You can see the very sweet fruits of the hard labour by running the following command in ProjectName folder:

.. code-block:: bash

   $ python main.py


You should see the following window:


.. image:: _static/example_window.png


Directory structure
--------------------------------

The newly created directory has the following structure::

  ProjectName/
    -- data/
    -- panels/
    -- plots/
    -- views/
    -- config.py
    -- main.py
    -- __init__.py_

This the ``data``, ``panels``, ``plots`` refer to data-exploration-plots pattern of monitoring.
The ``views`` folder is used to glue all those parts together.

You can check the code in the ``implementation`` in the main repo to hack right away, or move on to tutorial page for more details
