Web
===================


This functionality is not yet fully developed, yet we encourage you to try it out.


Preparing existing code
-------------------------------

We will use the example created in previous page (:ref:`tutorial <tutorial>`).
We will have to modify the ``views/weather_gui.py``.
First of all we will rewrite the **WeatherGUI** class, and split it into two classes.
We will create **Weather** class that inherits from :py:class:`titania.common.titania_tab.TitaniaPlotTab`, and define it's **make_plot**, **get_title** methods, like so:

.. code-block:: python

    from panels.weather_panel import WeatherControlPanel
    from data.weather_data import  WeatherData, WeatherDataCached
    from plots.weather_plot import WeatherPlot
    from titania.common.titania_tab import TitaniaPlotTab

    class Weather(TitaniaPlotTab):
        def __init__(self, parent=None):
            self.parent = parent
            super().__init__(data=WeatherDataCached())

        def make_plot(self):
            return WeatherPlot(parent=self.parent, view=self)

        def get_title(self):
            return "Weather Plot"

The class above is **base class** for both GUI and Web views.
Then we use this class along with :py:class:`titania.QtGUI.base_tab.QtBaseLayoutTab`, to create GUI view.
We defince **create_control_panel** method inside.

.. code-block:: python

    from titania.QtGUI.base_tab import QtBaseLayoutTab

    class WeatherGUI(Weather, QtBaseLayoutTab):
        def __init__(self, parent=None):
            QtBaseLayoutTab.__init__(self, parent=parent)
            Weather.__init__(self, parent=parent)

        def create_control_panel(self):
            return WeatherControlPanel(widget=self)

Making new Web interface
-------------------------------

As of time of writing this, the minimal requirement for a class that enables plotting in web interface is as follows:

* inherit from :py:class:`titania.common.interface_tab.WebTab` or :py:class:`titania.web.base_tab.QtTitaniaWebTab`,
* the member of the class ``self.plot.figure`` should be a matplotlib's figure


So we can use :py:class:`titania.web.base_tab.QtTitaniaWebTab`, like so:


.. code-block:: python

    class WeatherWeb(Weather, QtTitaniaWebTab):
        def __init__(self, parent=None):
            self.parent = parent
            Weather.__init__(self, parent=parent)
            QtTitaniaWebTab.__init__(self)

The extension of the :py:class:`titania.web.base_tab.QtTitaniaWebTab` provides tight control over the process of creating the view.
But if you don't need advanced options you can just use :py:class:`titania.web.base_tab.WebMetaclass`
You can just define it using metaclass:

.. code-block:: python

    from titania.web.base_tab import WebMetaclass

    class WeatherWeb(Weather, metaclass=WebMetaclass):
        pass


Both of the methods are valid, and in fact both boil down to one requirement for plotting the

Putting it all together
-------------------------------

And that's it, your ``weather_gui.py`` should like this:

.. code-block:: python

    from panels.weather_panel import WeatherControlPanel
    from data.weather_data import  WeatherData, WeatherDataCached
    from plots.weather_plot import WeatherPlot
    from titania.common.titania_tab import TitaniaPlotTab
    from titania.QtGUI.base_tab import QtBaseLayoutTab
    from titania.web.base_tab import WebMetaclass

    class Weather(TitaniaPlotTab):
        def __init__(self, parent=None):
            self.parent = parent
            super().__init__(data=WeatherDataCached())

        def make_plot(self):
            return WeatherPlot(parent=self.parent, view=self)

        def get_title(self):
            return "Weather Plot"

        def create_control_panel(self):
            return WeatherControlPanel(widget=self)

    class WeatherGUI(Weather, QtBaseLayoutTab):
        def __init__(self, parent=None):
            QtBaseLayoutTab.__init__(self, parent=parent)
            Weather.__init__(self, parent=parent)

        def create_control_panel(self):
            return WeatherControlPanel(widget=self)

    class WeatherWeb(Weather, metaclass=WebMetaclass):
        pass


Setting up the web interface
-------------------------------

You will need additionally to create a seperate config file for the web interface, in ``config_web.py``:

.. code-block:: python


    from views.weather_gui import WeatherWeb
    config = {
        "ExampleTab": [WeatherWeb],
    }

And seperate main file ``main_web.py``

.. code-block:: python

    import config_web
    from titania.web.main_window import MainWindow

    if __name__ == '__main__':
        main_window = MainWindow(tab_config=config_web.config)
        main_window.run()


Your projects directory should look like this::

    ProjectName/
    -- data/
    -- panels/
    -- plots/
    -- views/
    -- config.py
    -- config_web.py
    -- main.py
    -- main_web.py
    -- __init__.py_

Running
-------------------------------

Now you can simply run ``python main_web.py``
The interface should work under following address: `http://localhost:5000 <http://localhost:5000>`_, you can open with web browser. (if it doesnt work check the programs output to see correct link if this one doesn't work)
After clicking in the link inside the website you should see something like this:

.. image:: _static/web_example.png
