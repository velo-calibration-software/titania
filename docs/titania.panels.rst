titania.panels package
======================

Submodules
----------

titania.panels.main\_control\_panel module
------------------------------------------

.. automodule:: titania.panels.main_control_panel
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: titania.panels
   :members:
   :undoc-members:
   :show-inheritance:
