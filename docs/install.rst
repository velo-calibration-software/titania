
Installation Guide
==================

The easiest way is to install ``titania`` from the `Python Package Index
<https://pypi.python.org/pypi/titania/>`_ using ``pip`` or ``easy_install``:

.. code-block:: bash

   $ pip install titania

The **recommended way** is to install the most fresh version of titania using pip and gitlab repo:

.. code-block:: bash

   $ pip install git+https://gitlab.cern.ch/velo-calibration-software/titania


For **developement** installation the recommended way is:

.. code-block:: bash

    $ git clone https://gitlab.cern.ch/velo-calibration-software/titania
    $ cd titania
    $ pip install -r requirements.txt
