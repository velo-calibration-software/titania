titania.data package
====================

Submodules
----------

titania.data.data\_core module
------------------------------

.. automodule:: titania.data.data_core
   :members:
   :undoc-members:
   :show-inheritance:

titania.data.examplary\_generated\_data module
----------------------------------------------

.. automodule:: titania.data.examplary_generated_data
   :members:
   :undoc-members:
   :show-inheritance:

titania.data.root\_data module
------------------------------

.. automodule:: titania.data.root_data
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: titania.data
   :members:
   :undoc-members:
   :show-inheritance:
